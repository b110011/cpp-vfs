# cpp-vfs

A simple virtual filesystem.

## Similar projects

- [anthony-y/tiny-vfs](https://github.com/anthony-y/tiny-vfs) - tiny virtual file system library for C++.
- [fgenesis/ttvfs](https://github.com/fgenesis/ttvfs) - a small C++ library for app-internal VFS support.
- [yevgeniy-logachev/vfspp](https://github.com/yevgeniy-logachev/vfspp) - a c++ virtual file system that allow to manipulate with files from memory, zip archives or native filesystems like it is just native filesystem.

## License

This library is distributed under the [Apache License, Version 2.0](LICENSE).
