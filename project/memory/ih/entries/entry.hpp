/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_IH_ENTRIES_ENTRY_HPP__
#define __B110011_FILESYSTEM_MEMORY_IH_ENTRIES_ENTRY_HPP__

#include "b110011/noncopyable.hpp"

#include <filesystem>

namespace b110011 {
namespace filesystem {
namespace memory {

class Directory;
class ModifiableVisitor;
class ReadOnlyVisitor;

class Entry
    :   private NonCopyable
{

public:

    [[nodiscard]]
    virtual std::uintmax_t getSize () const = 0;


    [[nodiscard]]
    virtual std::filesystem::path const & getName () const = 0;

    virtual void setName ( std::filesystem::path const & _newName ) = 0;


    [[nodiscard]]
    virtual std::filesystem::path getFullPath () const = 0;


    [[nodiscard]]
    virtual Directory const * getParentDirectory () const = 0;

    [[nodiscard]]
    virtual Directory * takeParentDirectory () = 0;

    virtual void setParentDirectory ( Directory * _pNewParent ) = 0;


    [[nodiscard]]
    virtual std::filesystem::file_time_type getLastModificationTime () const = 0;

    virtual void setLastModificationTime ( std::filesystem::file_time_type _newTime ) = 0;


    [[nodiscard]]
    virtual std::filesystem::file_type getType () const = 0;


    virtual void accept ( ModifiableVisitor & _visitor ) = 0;

    virtual void accept ( ReadOnlyVisitor & _visitor ) const = 0;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_IH_ENTRIES_ENTRY_HPP__
