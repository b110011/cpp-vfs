/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/visitors/directory_iterator_impl.hpp"

#include "ih/entries/directory.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

DirectoryIteratorImpl::DirectoryIteratorImpl ()
    :   m_result{ false }
{}

std::error_code
DirectoryIteratorImpl::getError () const noexcept
{
    return m_error;
}

bool
DirectoryIteratorImpl::iterate ( Entry const & _entry, Callback _callback )
{
    m_callback = std::move( _callback );

    _entry.accept( *this );

    return m_result;
}

void
DirectoryIteratorImpl::visit ( Directory const & _directory )
{
    m_result = _directory.forEachEntry( m_callback );
}

void
DirectoryIteratorImpl::visit ( File const & )
{
    m_error = std::make_error_code( std::errc::not_a_directory );
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
