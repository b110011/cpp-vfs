/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_DIRECTORYIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_DIRECTORYIMPL_HPP__

#include "b110011/string/comparators.hpp"
#include "b110011/string/hashers.hpp"

#include "ih/entries/directory.hpp"

#include "src/entries/entry_base_impl.hpp"

#include <unordered_map>

namespace b110011 {
namespace filesystem {
namespace memory {

class DirectoryImpl final
    :   public EntryBaseImpl< Directory >
{

public:

    DirectoryImpl ( Directory * _pParent, std::filesystem::path _name );

    ~DirectoryImpl () override;


    std::uintmax_t getSize () const override;


    bool hasEntry ( std::filesystem::path const & _name ) const override;


    Entry const * find ( std::filesystem::path const & _name ) const override;

    Entry * find ( std::filesystem::path const & _name ) override;


    bool forEachEntry ( EntryCallback _callback ) const override;


    bool add ( std::shared_ptr< Entry > _pEntry ) override;


    bool rename (
        std::filesystem::path const & _oldName,
        std::filesystem::path const & _newName
    ) override;


    bool remove ( std::filesystem::path const & _name ) override;

private:

    using BaseClass = EntryBaseImpl< Directory >;
    using PathView = std::basic_string_view< std::filesystem::path::value_type >;

    template< bool _IsCaseInsensitive >
    using EntriesContainer = std::unordered_map<
        PathView,
        std::shared_ptr< Entry >,
        StringHasher< _IsCaseInsensitive >,
        IsStringEqual< _IsCaseInsensitive >
    >;

    #ifdef _WIN32
    EntriesContainer< false > m_entries;
    #else
    EntriesContainer< true > m_entries;
    #endif

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_DIRECTORYIMPL_HPP__
