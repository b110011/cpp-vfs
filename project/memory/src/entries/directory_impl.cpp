/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/directory_impl.hpp"

#include <cassert>

namespace b110011 {
namespace filesystem {
namespace memory {

DirectoryImpl::DirectoryImpl ( Directory * _pParent, std::filesystem::path _name )
    :   BaseClass{
            _pParent,
            std::move( _name ),
            std::filesystem::file_type::directory
        }
{}

DirectoryImpl::~DirectoryImpl () = default;

std::uintmax_t
DirectoryImpl::getSize () const
{
    std::uintmax_t result{ 0 };
    for ( auto const & [ k, v ] : m_entries )
    {
        assert( v );
        result += v->getSize();
    }
    return result;
}

bool
DirectoryImpl::hasEntry ( std::filesystem::path const & _name ) const
{
    return m_entries.count( _name.native() );
}

Entry const *
DirectoryImpl::find ( std::filesystem::path const & _name ) const
{
    if ( auto it = m_entries.find( _name.native() ); it != m_entries.end() )
    {
        assert( it->second );
        return it->second.get();
    }

    return nullptr;
}

Entry *
DirectoryImpl::find ( std::filesystem::path const & _name )
{
    if ( auto it = m_entries.find( _name.native() ); it != m_entries.end() )
    {
        assert( it->second );
        return it->second.get();
    }

    return nullptr;
}

bool
DirectoryImpl::forEachEntry ( EntryCallback _callback ) const
{
    for ( auto const & [ k, v ] : m_entries )
    {
        assert( v );
        if ( !_callback( v ) )
        {
            return false;
        }
    }

    return true;
}

bool
DirectoryImpl::add ( std::shared_ptr< Entry > _pEntry )
{
    return m_entries.emplace( _pEntry->getName().native(), _pEntry ).second;
}

bool
DirectoryImpl::rename (
    std::filesystem::path const & _oldName,
    std::filesystem::path const & _newName
)
{
    if ( auto it = m_entries.find( _oldName.native() ); it != m_entries.end() )
    {
        assert( it->second );
        auto pEntry = it->second;

        pEntry->setName( _newName );
        add( pEntry );

        m_entries.erase( it );

        return true;
    }

    return false;
}

bool
DirectoryImpl::remove ( std::filesystem::path const & _name )
{
    if ( m_entries.erase( _name.native() ) )
    {
        return true;
    }

    return false;
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
