/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_STREAM_FILEDESCRIPTOR_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_STREAM_FILEDESCRIPTOR_HPP__

#include "b110011/filesystem/stream/stream_sentinel.hpp"

#include <sstream>

namespace b110011 {
namespace filesystem {
namespace memory {

class FileDescriptorsManager;

class FileDescriptor final
    :   public StreamSentinel
    ,   private std::stringbuf
    ,   private std::enable_shared_from_this< FileDescriptor >
{

public:

    FileDescriptor (
        FileDescriptorsManager & _manager,
        std::streambuf & _buffer,
        std::ios::openmode _mode
    );

    ~FileDescriptor () override;


    void attach ( std::ios & _stream );

    void detach ();

private:

    void makeShalowCopy ( std::streambuf & _buffer );

    void removeFromManager ();

private:

    FileDescriptorsManager & m_manager;

    std::ios * m_pStream;
    std::streambuf * m_pOldBuffer;
    std::ios::openmode m_mode;

    bool m_isClosed;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_STREAM_FILEDESCRIPTOR_HPP__
