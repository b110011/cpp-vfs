/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/stream/file_descriptor.hpp"

#include "src/stream/file_descriptors_manager.hpp"
#include "src/stream/stream_buffer_cracker.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

FileDescriptor::FileDescriptor (
    FileDescriptorsManager & _manager,
    std::streambuf & _buffer,
    std::ios::openmode _mode
)
    :   std::stringbuf{ _mode }
    ,   m_manager{ _manager }
    ,   m_pStream{ nullptr }
    ,   m_pOldBuffer{ nullptr }
    ,   m_mode{ _mode }
    ,   m_isClosed{ false }
{}

FileDescriptor::~FileDescriptor ()
{
    if ( m_isClosed )
    {
        return;
    }

    detach();
    removeFromManager();
}

void
FileDescriptor::attach ( std::ios & _stream )
{
    m_pStream = &_stream;
    m_pOldBuffer = m_pStream->rdbuf( this );
}

void
FileDescriptor::detach ()
{
    m_isClosed = true;
    m_pStream->rdbuf( m_pOldBuffer );
}

void
FileDescriptor::makeShalowCopy ( std::streambuf & _buffer )
{
    StreamBufferCracker cracker{ _buffer };

    setp( cracker.pbase(), cracker.epptr() );
    setg( cracker.eback(), cracker.gptr(), cracker.egptr() );
}

void
FileDescriptor::removeFromManager ()
{
    m_manager.removeHandler( shared_from_this(), m_mode );
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
