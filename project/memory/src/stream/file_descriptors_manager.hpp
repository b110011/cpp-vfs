/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_STREAM_FILEDESCRIPTORSMANAGER_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_STREAM_FILEDESCRIPTORSMANAGER_HPP__

#include "b110011/filesystem/stream/stream.hpp"

#include <unordered_set>

namespace b110011 {
namespace filesystem {
namespace memory {

class FileDescriptor;

class FileDescriptorsManager final
{

    friend class FileDescriptor;

public:

    FileDescriptorsManager ( std::streambuf & _mainFileBuffer );

    ~FileDescriptorsManager ();


    FileStream createStream (
        std::ios::openmode _mode,
        std::error_code _ec
    );

    InputFileStream createInputStream (
        std::ios::openmode _mode,
        std::error_code _ec
    );

    OutputFileStream createOutputStream (
        std::ios::openmode _mode,
        std::error_code _ec
    );

private:

    bool hasHandlers () const noexcept;


    template< typename _StreamType >
    Stream< _StreamType > createStream ( std::ios::openmode _mode );


    void removeHandler ( std::shared_ptr< FileDescriptor > _pHanlder, std::ios::openmode _mode );

    void removeAllHandlers ();


    static bool isModifiableMode ( std::ios::openmode _mode ) noexcept;

private:

    std::streambuf & m_mainFileBuffer;

    short m_inHandlersCount;
    bool m_hasOutDescriptor;
    bool m_hasInoutDescriptor;

    std::unordered_set< std::shared_ptr< FileDescriptor > > m_handlers;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_STREAM_FILEDESCRIPTORSMANAGER_HPP__
