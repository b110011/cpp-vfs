/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_MODEL_FILESYSTEMMODELIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_MODEL_FILESYSTEMMODELIMPL_HPP__

#include "b110011/filesystem/filesystem_model.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

class Root;

class FileSystemModelImpl final
    :   public FileSystemModel
{

public:

    FileSystemModelImpl ( std::shared_ptr< Root > _pRoot );

    ~FileSystemModelImpl () override;


    std::uintmax_t getFileSize (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const override;


    std::filesystem::file_time_type getLastWriteTime (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const override;

    void setLastWriteTime (
        std::filesystem::path const & _path,
        std::filesystem::file_time_type _newTime,
        std::error_code & _ec
    ) override;


    std::filesystem::space_info getSpaceInfo (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const override;


    bool isDirectory (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const override;

    bool isRegularFile (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const override;


    bool exists (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const override;


    bool forEachDirectoryEntry (
        std::filesystem::path const & _directoryPath,
        bool _useRecursiveTraversal,
        DirectoryEntryCallback _callback
    ) const override;


    void rename (
        std::filesystem::path const & _oldPath,
        std::filesystem::path const & _newPath,
        std::error_code & _ec
    ) override;


    bool remove (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) override;


    bool createDirectory (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) override;

private:

    std::shared_ptr< Root > m_pRoot;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_MODEL_FILESYSTEMMODELIMPL_HPP__
